#include <dht11.h>

dht11 DHT11 = dht11(D1, BUSA);//we are placing our devide on D1 on BUSA

void setup()
{
  Serial.begin(9600);
  Serial.println("Sicaklik ve Nem Olcme Devresi");
}

void loop()
{
  Serial.print("\n");

  int chk = DHT11.read();
 
  Serial.print("Nem    : %");
  Serial.println((float)DHT11.humidity, DEC);

  Serial.print("Derece : ");
  Serial.println((float)DHT11.temperature, DEC);

  delay(1000);
}
